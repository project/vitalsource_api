<?php

namespace Drupal\vitalsource_api;

use Drupal\Core\Site\Settings;
use GuzzleHttp\Client as Guzzle6Client;
use Http\Adapter\Guzzle6\Client as AdapterClient;
use Nymediaas\Vitalsource\Client;

/**
 * VitalsourceApiClient service.
 */
class VitalsourceApiClientFactory {

  const SETTINGS_NAME = 'vitalsource_api';

  /**
   * Client.
   *
   * @var \Nymediaas\Vitalsource\Client
   */
  protected $client;

  /**
   * Constructs a VitalsourceApiClient object.
   */
  public function __construct(Guzzle6Client $http_client) {
    $adapter = new AdapterClient($http_client);
    $this->client = new Client($adapter);
  }

  /**
   * Get the client directly.
   */
  public function getClient() {
    $settings = Settings::get(self::SETTINGS_NAME);
    if (empty($settings)) {
      throw new \InvalidArgumentException('No vital source settings found');
    }
    $this->client->setLiveMode($settings['live_mode']);
    $this->client->setApiKey($settings['api_key']);
    return $this->client;
  }

}
